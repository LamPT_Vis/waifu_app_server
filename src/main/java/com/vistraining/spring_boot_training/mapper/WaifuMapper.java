package com.vistraining.spring_boot_training.mapper;

import com.vistraining.spring_boot_training.DTO.WaifuDTO;
import com.vistraining.spring_boot_training.models.Waifu;
import java.util.List;
import org.mapstruct.Mapper;

@Mapper
public interface WaifuMapper {

  public WaifuDTO toWaifuDTO(Waifu waifu);

  public List<WaifuDTO> toWaifuDTOs(List<Waifu> waifus);

  public Waifu toWaifu(WaifuDTO waifuDTO);

  public List<Waifu> toWaifus(List<WaifuDTO> waifuDTOS);
}
