package com.vistraining.spring_boot_training.aspect;

import java.util.Arrays;
import lombok.extern.log4j.Log4j2;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Component
@Log4j2
@Aspect
public class LoggingAspect {

  // That means this advice wil be performed before any method has this signature
  @Before("inUtilLayer()")
  public void enteringLogging(JoinPoint joinPoint) {
    log.info(
        "Entering method: [" + joinPoint.getTarget().getClass().getSimpleName() + "." + joinPoint
            .getSignature().getName() + "]");
    log.info("With args: " + Arrays.toString(joinPoint.getArgs()));
  }

  // This will be performed after a method returns
  @AfterReturning(pointcut = "inUtilLayer()", returning = "result")
  public void logAfterReturning(JoinPoint joinPoint, Object result) {
    log.info(
        "Exiting method: [" + joinPoint.getTarget().getClass().getSimpleName() + "." + joinPoint
            .getSignature().getName() + "]");
    log.info("With returned data: " + result);
  }

  // This will be performed whenever a method throws an exception
  @AfterThrowing(pointcut = "inUtilLayer()", throwing = "error")
  public void logAfterThrowing(JoinPoint joinPoint, Throwable error) {
    log.error(error);
  }

  // This is a point cut to determine what to intercept
  @Pointcut("within(com.vistraining.spring_boot_training.controllers..*)")
  public void inUtilLayer() {
  }
// Add other layers if any
  //source: https://haythamsalhi.wordpress.com/2015/04/23/log4j2-integration-with-aspectj-in-web-application-using-tomcat-8-server/
}
