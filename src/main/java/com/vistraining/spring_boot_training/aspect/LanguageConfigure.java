package com.vistraining.spring_boot_training.aspect;

import java.util.Locale;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;

@Configuration
public class LanguageConfigure implements WebMvcConfigurer {

  @Bean(name="localeResolver")
  public LocaleResolver getLocaleResolver(){
    CookieLocaleResolver localeResolver=new CookieLocaleResolver();
    localeResolver.setCookieDomain("myLocaleCookie");
    localeResolver.setDefaultLocale(Locale.ENGLISH);
    localeResolver.setCookieMaxAge(60*60);
    return localeResolver;
  }

  @Bean(name="messageSource")
  public MessageSource getMessageSource(){
    ReloadableResourceBundleMessageSource messageBundle=new ReloadableResourceBundleMessageSource();

    messageBundle.setBasename("classpath:msg/message");
    messageBundle.setDefaultEncoding("UTF-8");
    return messageBundle;
  }

  @Override
  public void addInterceptors(InterceptorRegistry registry) {
    LocaleChangeInterceptor interceptor=new LocaleChangeInterceptor();
    interceptor.setParamName("lang");
    registry.addInterceptor(interceptor).addPathPatterns("/*");
  }
}
