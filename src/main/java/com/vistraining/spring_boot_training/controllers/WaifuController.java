package com.vistraining.spring_boot_training.controllers;

import com.vistraining.spring_boot_training.DTO.WaifuDTO;
import com.vistraining.spring_boot_training.custom_exceptions.ResourceNotFoundException;
import com.vistraining.spring_boot_training.mapper.WaifuMapper;
import com.vistraining.spring_boot_training.models.Waifu;
import com.vistraining.spring_boot_training.repositories.WaifuRepository;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Log4j2
@CrossOrigin
@RequiredArgsConstructor
@RestController
@RequestMapping("/waifu")
@Validated
public class WaifuController {

  SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();

  @Autowired
  private WaifuRepository waifuRepository;

  private final WaifuMapper waifuMapper;

  @Autowired
  private MessageSource messageSource;

  @GetMapping("/infor_waifu")
  public List<Waifu> getAllWaifu(HttpServletRequest request) throws ResourceNotFoundException {

    String lang=request.getParameter("lang");
    String message="";
    if(lang.trim().equals("en")){
      message=messageSource.getMessage("nothing_here",null,"default",Locale.ENGLISH);
    }else{
      message=messageSource.getMessage("nothing_here",null,"default",request.getLocale());
    }
    List<Waifu> res = waifuRepository.findAll();
    if (res.size() == 0) {
      throw new ResourceNotFoundException(message);
    }
    return res;
  }

//  @GetMapping("/infor_waifu")
//  public ResponseEntity<List<WaifuDTO>> getAllWaifu() throws ResourceNotFoundException {
//    log.info("GET ALL REQUEST");
//    List<Waifu> res = waifuRepository.findAll();
//    if (res.size() == 0) {
//      ResourceNotFoundException e = new ResourceNotFoundException("Nothing here!!!");
//      log.error(e.getMessage(), e);
//      throw e;
//    }
//    log.info(new Gson().toJson(res));
//    return ResponseEntity.ok(waifuMapper.toWaifuDTOs(res));
//  }

  @GetMapping("/infor_waifu/{id}")
  public ResponseEntity<Waifu> getAWaifuById(HttpServletRequest request,@PathVariable(value = "id") Long id)
      throws ResourceNotFoundException {
    String lang=request.getParameter("lang");
    String message="";
    if(lang.trim().equals("en")){
      message=messageSource.getMessage("your_waifu_not_existed",null,"default",Locale.ENGLISH);
    }else{
      message=messageSource.getMessage("your_waifu_not_existed",null,"default",request.getLocale());
    }
    try{
      Optional<Waifu> res=waifuRepository.findById(id);
      return ResponseEntity.ok().body(res.get());
    }catch(Exception e){
      throw new ResourceNotFoundException(message);
    }
  }

  @GetMapping("/infor_waifu/name/{name}")
  public ResponseEntity<Waifu> getWaifuByName(HttpServletRequest request,@PathVariable(value = "name") String name)
      throws ResourceNotFoundException {
    String lang=request.getParameter("lang");
    String message="";
    if(lang.trim().equals("en")){
      message=messageSource.getMessage("who_are_you_looking_for",null,"default",Locale.ENGLISH);
    }else{
      message=messageSource.getMessage("who_are_you_looking_for",null,"default",request.getLocale());
    }
    log.info(message);
    Session session = sessionFactory.openSession();
    String hql = " from Waifu w where w.name like :key";
    log.info("QUERY: SELECT WAIFU FROM WAIFU WHERE WAIFU.NAME LIKE %" + name + "%");
    Waifu waifu = session.createQuery(hql, Waifu.class).setParameter("key", "%" + name + "%")
        .uniqueResult();
    if (waifu == null) {
      throw  new ResourceNotFoundException(
          message);
    }
    return ResponseEntity.ok().body(waifu);
  }

  @GetMapping("/infor_waifu/name")
  public ResponseEntity<List<WaifuDTO>> getAllWaifuName() throws ResourceNotFoundException {
    List<Waifu> waifusName =waifuRepository.findAll();
    if (waifusName.size() == 0) {
      throw new ResourceNotFoundException("Nothing here!!");
    }
    return ResponseEntity.ok(waifuMapper.toWaifuDTOs(waifusName));
  }

  @PostMapping("/add_waifu")
  public Waifu addWaifu(@Validated({BHWinfor.class}) @RequestBody Waifu waifu) {
    return waifuRepository.save(waifu);
  }
}
