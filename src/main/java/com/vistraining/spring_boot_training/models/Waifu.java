package com.vistraining.spring_boot_training.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.gson.Gson;
import com.vistraining.spring_boot_training.controllers.BHWinfor;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import lombok.Data;

@Data
@Entity
@Table(name = "tbl_waifu")
public class Waifu {

  @Id
  @JsonIgnore
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_WAIFU_SEQ")
  @SequenceGenerator(name = "TBL_WAIFU_SEQ", sequenceName = "TBL_WAIFU_SEQ", allocationSize = 1)
  private long id;

  @Column(name = "name", nullable = false)
  private String name;

  @Column(name = "b_param", nullable = false)
  @Min(value = 60, message = "your waifu is a flat-chest tsundere, right?", groups = BHWinfor.class)
  private int bParam;

  @Column(name = "h_param", nullable = false)
  @Min(value = 40, message = "man, that is a loli waist, unacceptable!", groups = BHWinfor.class)
  private int hParam;

  @Column(name = "w_param", nullable = false)
  @Min(value = 60, message = "That is illegal, ya know!", groups = BHWinfor.class)
  private int wParam;

  @Column(name = "appearance")
  private String appearance;

  @Column(name = "characteristic")
  private String characteristic;

  @Column(name = "alias")
  private String alias;

  @Column(name = "img_src", nullable = false)
  private String imgSrc;

  public Waifu() {

  }

  public Waifu(long id, String name, int bParam, int hParam, int wParam, String appearance,
      String characteristic, String alias, String imgSrc) {
    this.id = id;
    this.name = name;
    this.bParam = bParam;
    this.hParam = hParam;
    this.wParam = wParam;
    this.appearance = appearance;
    this.characteristic = characteristic;
    this.alias = alias;
    this.imgSrc = imgSrc;
  }

  public Waifu(String name, int bParam, int hParam, int wParam, String appearance,
      String characteristic, String alias, String imgSrc) {
    this.name = name;
    this.bParam = bParam;
    this.hParam = hParam;
    this.wParam = wParam;
    this.appearance = appearance;
    this.characteristic = characteristic;
    this.alias = alias;
    this.imgSrc = imgSrc;
  }

  public Waifu(String name, int bParam, int hParam, int wParam) {
    this.name = name;
    this.bParam = bParam;
    this.hParam = hParam;
    this.wParam = wParam;
  }

  public Waifu(long id, String name, int bParam, int hParam, int wParam) {
    this.id = id;
    this.name = name;
    this.bParam = bParam;
    this.hParam = hParam;
    this.wParam = wParam;
  }


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getbParam() {
    return bParam;
  }

  public void setbParam(int bParam) {
    this.bParam = bParam;
  }

  public int gethParam() {
    return hParam;
  }

  public void sethParam(int hParam) {
    this.hParam = hParam;
  }

  public int getwParam() {
    return wParam;
  }

  public void setwParam(int wParam) {
    this.wParam = wParam;
  }

  public String getAppearance() {
    return appearance;
  }

  public void setAppearance(String appearance) {
    this.appearance = appearance;
  }

  public String getCharacteristic() {
    return characteristic;
  }

  public void setCharacteristic(String characteristic) {
    this.characteristic = characteristic;
  }

  public String getAlias() {
    return alias;
  }

  public void setAlias(String alias) {
    this.alias = alias;
  }

  public String getImgSrc() {
    return imgSrc;
  }

  public void setImgSrc(String imgSrc) {
    this.imgSrc = imgSrc;
  }

  public String toString() {
    return new Gson().toJson(this);
  }
}
