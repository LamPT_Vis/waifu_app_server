package com.vistraining.spring_boot_training.repositories;

import com.vistraining.spring_boot_training.models.Waifu;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WaifuRepository extends JpaRepository<Waifu, Long> {

}
