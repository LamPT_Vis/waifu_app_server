package com.vistraining.spring_boot_training.DTO;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class WaifuDTO {

  @JsonProperty("Name")
  private String name;
  private int bParam;
  private int hParam;
  private int wParam;
  private String appearance;
  private String characteristic;
  private String alias;
  @JsonIgnore
  private String imgSrc;

  @JsonValue
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getbParam() {
    return bParam;
  }

  public void setbParam(int bParam) {
    this.bParam = bParam;
  }

  public int gethParam() {
    return hParam;
  }

  public void sethParam(int hParam) {
    this.hParam = hParam;
  }

  public int getwParam() {
    return wParam;
  }

  public void setwParam(int wParam) {
    this.wParam = wParam;
  }

  public String getAppearance() {
    return appearance;
  }

  public void setAppearance(String appearance) {
    this.appearance = appearance;
  }

  public String getCharacteristic() {
    return characteristic;
  }

  public void setCharacteristic(String characteristic) {
    this.characteristic = characteristic;
  }

  public String getAlias() {
    return alias;
  }

  public void setAlias(String alias) {
    this.alias = alias;
  }

  public String getImgSrc() {
    return imgSrc;
  }

  public void setImgSrc(String imgSrc) {
    this.imgSrc = imgSrc;
  }

}
